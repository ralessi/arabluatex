;;; arabluatex.el --- AUCTeX style for `arabluatex.sty'
;; This file is part of the `arabluatex' package

;; ArabLuaTeX -- Processing ArabTeX notation under LuaLaTeX
;; Copyright (C) 2016--2022  Robert Alessi

;; The author would like to express his grateful thanks to Arash
;; Esbati <arash@gnu.org> whose comments helped to improve this file
;; immensely.

;; Please send error reports and suggestions for improvements to Robert
;; Alessi <alessi@robertalessi.net>

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <http://www.gnu.org/licenses/>.

(defvar LaTeX-arabluatex-preamble-options
  '(("voc")
    ("fullvoc")
    ("novoc")
    ("trans")
    ("export" ("true" "false")))
  "Package options for the arabluatex package.")

(defun LaTeX-arabluatex-package-options ()
  "Prompt for package options for arabluatex package."
  (TeX-read-key-val t LaTeX-arabluatex-preamble-options))

(defvar LaTeX-arabluatex-mode-options
  '(("voc")
    ("fullvoc")
    ("novoc")
    ("trans"))
  "List of local options for arabluatex macros.")

(defvar LaTeX-arabluatex-key-val-options
  '(("mode" ("voc" "fullvoc" "novoc" "trans"))
    ("width")
    ("gutter")
    ("metre")
    ("color")
    ("delim" ("true" "false"))
    ("utf" ("true" "false"))
    ("export" ("true" "false")))
  "Key=value options for arabverse environment.")

(defvar LaTeX-arabluatex-outfile-options
  '(("newline"))
  "List of options for outfile command.")

(defvar LaTeX-arabluatex-arbmark-options
  '(("rl")
    ("lr"))
  "List of options for arbmark command.")

(defvar LaTeX-arabluatex-arind-options
  '(("index")
    ("root"
     ("1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15"))
    ("form")
    ("pipe"))
  "Option for arind command.")

(TeX-add-style-hook
 "arabluatex"
 (lambda ()
   ;; Run the style hook for loaded packages:
   (TeX-run-style-hooks "xcolor")
   ;; Folding features:
   (add-to-list (make-local-variable 'LaTeX-fold-macro-spec-list)
		'("[i]" ("arind")) t)
   ;; This package relies on lualatex, so check for it:
   (TeX-check-engine-add-engines 'luatex)
   (TeX-add-symbols
    "aemph"
    "SetInputScheme"
    '("SetArbEasy" 0)
    '("SetArbDflt" 0)
    '("SetArbEasy*" 0)
    '("SetArbDflt*" 0)
    "SetTranslitFont"
    "SetTranslitStyle"
    "SetTranslitConvention"
    "SetArbOutSuffix"
    "arbup"
    '("ArbUpDflt" 0)
    '("NoArbUp" 0)
    "SetArbUp"
    "uc"
    "prname"
    "txarb"
    '("arb" [ TeX-arg-eval completing-read
			   (TeX-argument-prompt nil nil "Mode")
			   LaTeX-arabluatex-mode-options ]
      t)
    '("arbcolor" [ TeX-arg-eval completing-read
				(TeX-argument-prompt nil nil "Color")
				(LaTeX-xcolor-definecolor-list) ]
      t)
    '("arbmark" [ TeX-arg-eval completing-read
			       (TeX-argument-prompt nil nil "Option")
			       LaTeX-arabluatex-arbmark-options ]
      t)
    '("ArbOutFile" [ TeX-arg-eval completing-read
				  (TeX-argument-prompt nil nil "Option")
				  LaTeX-arabluatex-outfile-options ]
      t)
    '("ArbOutFile*" [ TeX-arg-eval completing-read
				   (TeX-argument-prompt nil nil "Option")
				   LaTeX-arabluatex-outfile-options ]
      t)
    '("arind" [ TeX-arg-key-val LaTeX-arabluatex-arind-options ]
      TeX-arg-index)
    "SetHemistichDelim"
    '("bayt" 2)
    "abjad"
    '("SetArbNumbers" "Indian or Arabic")
    "arbnull"
    "abraces"
    "LR"
    "RL"
    '("LRmarginpar" [ "Left margin text" ] "Text")
    '("LRfootnote"
      (TeX-arg-conditional TeX-arg-footnote-number-p ([ "Number" ]) nil)
      t)
    '("RLfootnote"
      (TeX-arg-conditional TeX-arg-footnote-number-p ([ "Number" ]) nil)
      t)
    "FixArbFtnmk"
    "MkArbBreak"
    '("newarbmark" 3)
    '("setRL" 0)
    '("setLR" 0))

   (LaTeX-add-environments
    '("arab" LaTeX-env-args
      [ TeX-arg-eval completing-read
		     (TeX-argument-prompt nil nil "Mode")
		     LaTeX-arabluatex-mode-options ]
      )
    "txarab"
    "arabexport"
    '("arabverse" LaTeX-env-args
      [ TeX-arg-key-val LaTeX-arabluatex-key-val-options ] ))

   ;; Fontification
   (when (and (featurep 'font-latex)
	      (eq TeX-install-font-lock 'font-latex-setup))
     (font-latex-add-keywords '(("aemph"  "{"))
			      'italic-command)
     (font-latex-add-keywords '(("LRmarginpar" "[{")
				("LRfootnote"  "[{")
				("RLfootnote"  "[{")
				("arind"       "[{"))
			      'reference)
     (font-latex-add-keywords '(("SetTranslitFont"       "{")
				("SetTranslitStyle"      "{")
				("SetTranslitConvention" "{"))
			      'function))
   ;; RefTeX support
   (when (fboundp 'reftex-add-index-macros)
     (reftex-add-index-macros '(("\\arind[]{*}" "" ?a "" nil t))))
   )
 LaTeX-dialect)

;;; arabluatex.el ends here
